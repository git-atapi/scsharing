import socket
import struct
from PIL import ImageGrab
import io
from utils import compress
import threading

def capture_screen():
    """Capture the screen and return the compressed binary data."""
    screen = ImageGrab.grab()
    with io.BytesIO() as output:
        screen.save(output, format="JPEG", quality=85)
        return compress(output.getvalue())

def handle_client(conn):
    """Handle the client connection to receive screen captures."""
    try:
        while True:
            screen_data = capture_screen()
            conn.sendall(struct.pack(">L", len(screen_data)))
            conn.sendall(screen_data)
    except Exception as e:
        print(f"Connection closed: {e}")
    finally:
        conn.close()

def server():
    """Start the screen sharing server."""
    host = "127.0.0.1"
    port = 5000
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen()
        print(f"Server listening on {host}:{port}")
        while True:
            conn, addr = s.accept()
            print(f"Connected by {addr}")
            threading.Thread(target=handle_client, args=(conn,)).start()

if __name__ == "__main__":
    server()
