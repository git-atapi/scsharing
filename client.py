import sys
from PySide2.QtWidgets import QApplication, QLabel, QMainWindow
from PySide2.QtGui import QPixmap, QImage
import socket
import struct
from utils import decompress

class ScreenViewer(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Screen Viewer")
        self.setGeometry(100, 100, 800, 600)
        self.label = QLabel(self)
        self.label.resize(800, 600)
        self.show()

    def update_image(self, image):
        """Update the displayed image."""
        qimg = QImage.fromData(image)
        pixmap = QPixmap.fromImage(qimg)
        self.label.setPixmap(pixmap.scaled(self.label.size(), self.label.size(), aspectRatioMode=Qt.IgnoreAspectRatio))

def client(host="127.0.0.1", port=5000):
    """Connect to the screen sharing server and display the received screen captures."""
    app = QApplication(sys.argv)
    viewer = ScreenViewer()
    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        while True:
            size = struct.unpack(">L", s.recv(4))[0]
            img_data = b""
            while len(img_data) < size:
                packet = s.recv(size - len(img_data))
                if not packet:
                    return
                img_data += packet
            img_data = decompress(img_data)
            viewer.update_image(img_data)
            QApplication.processEvents()

if __name__ == "__main__":
    client()
